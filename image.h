#pragma once

#include <stdint.h>
#include "bmp.h"

struct pixel {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

struct image {
    uint32_t width;
    uint32_t height;

    struct pixel * pixels;
};

struct image image_from_bmp(struct bmp_image *bmp_image);

void bmp_from_image(struct bmp_image * bmp_image, const struct image image);

struct pixel * pixel_of(const struct image image, uint64_t x, uint64_t y);

struct image sepia_c_inplace( const struct image image );

struct pixel sepia_asm( struct pixel* const pixel );

struct image sepia_asm_inplace( const struct image image );
