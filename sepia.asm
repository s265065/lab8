%define c_r1 0.393
%define c_r2 0.349
%define c_r3 0.272
%define c_g1 0.769
%define c_g2 0.686
%define c_g3 0.543
%define c_b1 0.189
%define c_b2 0.168
%define c_b3 0.131

section .rodata
align 16
    red:      dd c_r1, c_r2, c_r3, c_r1, c_r2, c_r3, c_r1, c_r2, c_r3, c_r1, c_r2, c_r3
    green:    dd c_g1, c_g2, c_g3, c_g1, c_g2, c_g3, c_g1, c_g2, c_g3, c_g1, c_g2, c_g3
    blue:     dd c_b1, c_b2, c_b3, c_b1, c_b2, c_b3, c_b1, c_b2, c_b3, c_b1, c_b2, c_b3

; rdi = r, rsi = g, rdx = b, rcx = result

section .text
global sepia

sepia:
    movdqu xmm0, [rdi]
    movdqu xmm1, [rdi + 16]
    movdqu xmm2, [rdi + 32]
    movdqu xmm3, [rsi]
    movdqu xmm4, [rsi + 16]
    movdqu xmm5, [rsi + 32]
    movdqu xmm6, [rdx]
    movdqu xmm7, [rdx + 16]
    movdqu xmm8, [rdx + 32]

    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2
    cvtdq2ps xmm3, xmm3
    cvtdq2ps xmm4, xmm4
    cvtdq2ps xmm5, xmm5
    cvtdq2ps xmm6, xmm6
    cvtdq2ps xmm7, xmm7
    cvtdq2ps xmm8, xmm8

    mulps xmm0, [rel red]
    mulps xmm1, [rel red+16]
    mulps xmm2, [rel red+32]
    mulps xmm3, [rel green]
    mulps xmm4, [rel green+16]
    mulps xmm5, [rel green+32]
    mulps xmm6, [rel blue]
    mulps xmm7, [rel blue+16]
    mulps xmm8, [rel blue+32]

    addps xmm0, xmm3
    addps xmm0, xmm6
    addps xmm1, xmm4
    addps xmm1, xmm7
    addps xmm2, xmm5
    addps xmm2, xmm8

    cvtps2dq xmm0, xmm0
    cvtps2dq xmm1, xmm1
    cvtps2dq xmm2, xmm2

    xorps xmm3, xmm3

    packusdw xmm0, xmm1
    packusdw xmm2, xmm3

    packuswb xmm0, xmm2

    movdqu [rcx], xmm0

    ret