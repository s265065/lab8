C_FLAGS = -ansi -lm -c -std=c99 -pedantic -Wall -Werror
ASM_FLAGS = -felf64 -o
compile:
	nasm $(ASM_FLAGS) sepia.o sepia.asm
	gcc $(C_FLAGS) bmp.c -o bmp.o
	gcc $(C_FLAGS) image.c -o image.o
	gcc $(C_FLAGS) main.c -o main.o
	gcc *.o -o main
remove:
	rm -f main
