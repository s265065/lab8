#include <stdlib.h>
#include <stdint.h>

#include "bmp.h"
#include "image.h"

#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

int main(int argc, char **argv) {
    int i;

    for (i = 0; i < argc; i++) {
        printf("%s\n", argv[i]);
    }

    char * input_filename = argv[1];
    char * output_filename = argv[2];

    FILE * file;
    struct bmp_image *bmp_image = malloc(sizeof(struct bmp_image));

    file = fopen(input_filename, "rb");
    bmp_read(bmp_image, file);
    fclose(file);

    struct image image = image_from_bmp(bmp_image);

        struct rusage r;
        struct timeval start;
        struct timeval end;
        getrusage(RUSAGE_SELF, &r );
        start = r.ru_utime;
    struct image  sepia_image = sepia_asm_inplace(image);
        getrusage(RUSAGE_SELF, &r );
        end = r.ru_utime;
        long res = ((end.tv_sec - start.tv_sec) * 1000000L) +
                   end.tv_usec - start.tv_usec;
        printf( "Time elapsed in microseconds asm: %ld\n", res );

    getrusage(RUSAGE_SELF, &r );
    start = r.ru_utime;
    sepia_image = sepia_c_inplace(image);
    getrusage(RUSAGE_SELF, &r );
    end = r.ru_utime;
    res = ((end.tv_sec - start.tv_sec) * 1000000L) +
               end.tv_usec - start.tv_usec;
    printf( "Time elapsed in microseconds c: %ld\n", res );

    bmp_from_image(bmp_image, sepia_image);

    file = fopen(output_filename, "wb");
    bmp_write(*bmp_image, file);

    return 0;
}
