#include "image.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>

double max(double a, double b){
    return a > b ? a : b;
}

double min(double a, double b){
    return a < b ? a : b;
}


struct image image_from_bmp(struct bmp_image *bmp_image) {
    struct image image;
    uint32_t size = bmp_image->header.biWidth * bmp_image->header.biHeight;

    image.width = bmp_image->header.biWidth;
    image.height = bmp_image->header.biHeight;
    image.pixels = malloc(sizeof(struct pixel) * image.width * image.height);

    uint32_t i;
    for (i = 0; i < size; ++i) {
        image.pixels[i].red = bmp_image->pixels[i].r;
        image.pixels[i].green = bmp_image->pixels[i].g;
        image.pixels[i].blue = bmp_image->pixels[i].b;
    }

    return image;
}

void bmp_from_image(struct bmp_image * bmp_image, const struct image image) {

    bmp_image->header.bfType[0] = 'B';
    bmp_image->header.bfType[1] = 'M';

    bmp_image->header.bfOffBits = sizeof(struct bmp_header);

    bmp_image->header.biSize = 40;
    bmp_image->header.biPlanes = 1;
    bmp_image->header.biBitCount = 24;
    bmp_image->header.biCompression = 0;

    bmp_image->header.biWidth = image.width;
    bmp_image->header.biHeight = image.height;

    bmp_image->header.biSizeImage = bmp_image->header.biHeight *
                                    (bmp_image->header.biWidth * sizeof(struct bmp_pixel)
                                     + bmp_image->header.biWidth % 4);

    bmp_image->header.bfSize = bmp_image->header.bfOffBits + bmp_image->header.biSizeImage;

    uint32_t i;
    for (i = 0; i <  bmp_image->header.biHeight*bmp_image->header.biWidth; ++i) {
        bmp_image->pixels[i].b = image.pixels[i].blue ;
        bmp_image->pixels[i].g = image.pixels[i].green;
        bmp_image->pixels[i].r = image.pixels[i].red;
    }

}

struct pixel * pixel_of(const struct image img, uint64_t x, uint64_t y) {
    return &img.pixels[y * img.width + x];
}

static unsigned char sat( uint64_t x) {
    if (x < 256) return x;
    return 255;
}

struct pixel sepia_one( struct pixel* const pixel ) {
    static const float c[3][3] = {
            { .393f, .769f, .189f },
            { .349f, .686f, .168f },
            { .272f, .543f, .131f } };

    struct pixel new_pixel;

    new_pixel.red = sat(
            pixel->red * c[0][0] + pixel->green * c[0][1] + pixel->blue * c[0][2]);
    new_pixel.green = sat(
            pixel->red * c[1][0] + pixel->green * c[1][1] + pixel->blue * c[1][2]);
    new_pixel.blue = sat(
            pixel->red * c[2][0] + pixel->green * c[2][1] + pixel->blue * c[2][2]);

    return new_pixel;
}

struct image sepia_c_inplace(const struct image image ) {

    struct image new_image;

    uint32_t x;
    uint32_t y;

    new_image.width = image.width;
    new_image.height = image.height;

    new_image.pixels = malloc(sizeof(struct pixel) * new_image.width * new_image.height);

    for( y = 0; y < image.height; y++ )
        for( x = 0; x < image.width; x++ )
        new_image.pixels[y * new_image.width + x] = sepia_one( pixel_of( image, x, y ) );

    return new_image;
}

void sepia( uint32_t[12], uint32_t[12], uint32_t [12], uint8_t [16]);

static void process_cluster(struct pixel * cluster, uint8_t size) {
    static uint32_t r[12], g[12], b[12];
    uint8_t i, j;
    uint8_t result[16];

    for (i = 0; i < size; ++i) {
        for (j = 0; j < 3; ++j) {
            r[i * 3 + j] = cluster[i].red;
            g[i * 3 + j] = cluster[i].green;
            b[i * 3 + j] = cluster[i].blue;
        }
    }

    sepia(r, g, b, result);

    memcpy(cluster, result, size * 3);
}

struct image sepia_asm_inplace(const struct image image ) {

    const uint32_t length = image.width * image.height;
    const uint32_t clusters = length / 4;
    uint32_t i;

    for (i = 0; i < clusters; ++i) {
        process_cluster(image.pixels + i * 4, 4);
    }

    if (length % 4 > 0) {
        process_cluster(image.pixels + i * 4, length % 4);
    }

    return image;
}
