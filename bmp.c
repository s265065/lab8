#include "bmp.h"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>


const char * bmp_read(struct bmp_image * image, FILE * file) {
    int32_t row, rowOffset;

    size_t count = fread(&(image->header), sizeof(struct bmp_header), 1, file);

    if (count < 1) {
        return "could not read file";
    }

    /* Check file type signature */
    if (image->header.bfType[0] != 'B' || image->header.bfType[1] != 'M') {
        return "wrong file";
    }

    /* Go to bitmap */
    if (fseek(file, image->header.bfOffBits, SEEK_SET)) {
        return strerror(errno);
    }

    image->pixels = malloc(sizeof(struct bmp_pixel) * image->header.biWidth * image->header.biHeight);

    rowOffset = image->header.biWidth % 4;
    for (row = image->header.biHeight - 1; row >= 0; --row) {
        count = fread(image->pixels + row * image->header.biWidth, sizeof(struct bmp_pixel), image->header.biWidth, file);

        if (count < image->header.biWidth) {
            free(image->pixels);
            return "could not read file";
        }

        if (fseek(file, rowOffset, SEEK_CUR)) {
            free(image->pixels);
            return strerror(errno);
        }
    }

    return NULL;
}


const char * bmp_write(const struct bmp_image image, FILE * file) {
    static uint8_t offsetBuffer[] = { 0, 0, 0 };
    int32_t row, rowOffset;

    size_t count = fwrite(&(image.header), sizeof(struct bmp_header), 1, file);

    if (count < 1) {
        return "could not write file";
    }

    rowOffset = image.header.biWidth % 4;
    for (row = image.header.biHeight - 1; row >= 0; --row) {
        count = fwrite(image.pixels + row * image.header.biWidth, sizeof(struct bmp_pixel), image.header.biWidth, file);

        if (count < image.header.biWidth) {
            return strerror(errno);
        }

        if (fwrite(offsetBuffer, 1, rowOffset, file) < rowOffset) {
            return strerror(errno);
        }

    }

    return NULL;
}
